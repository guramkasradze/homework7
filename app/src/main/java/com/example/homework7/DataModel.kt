package com.example.homework7

import com.google.gson.annotations.SerializedName

class DataModel {
    var data:MutableList<Data> = ArrayList()

    class Data {
        @SerializedName("id")
        var id : Int = 0
        @SerializedName("descriptionEN")
        val descriptionEN : String = ""
        @SerializedName("descriptionKA")
        val descriptionKA : String = ""
        @SerializedName("descriptionRU")
        val descriptionRU : String = ""
        @SerializedName("titleEN")
        val titleEN : String = ""
        @SerializedName("titleKA")
        val titleKA : String = ""
        @SerializedName("titleRU")
        val titleRU : String = ""
        @SerializedName("published")
        val published : Int = 0
        @SerializedName("publish_date")
        val publish_date : String = ""
        @SerializedName("created_at")
        val created_at : Long = 0
        @SerializedName("updated_at")
        val updated_at : Long = 0
        @SerializedName("category")
        val category : String = ""
        @SerializedName("cover")
        val cover : String = ""
        @SerializedName("isLast")
        val isLast : Boolean = false
    }
}