package com.example.homework7

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.homework7.databinding.ItemRecyclerviewLayoutBinding
import kotlinx.android.synthetic.main.item_recyclerview_layout.view.*

class RecyclerViewAdapter(private val items: MutableList<DataModel.Data>, private val activity: MainActivity): RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(DataBindingUtil.inflate<ItemRecyclerviewLayoutBinding>(
        LayoutInflater.from(parent.context),
        R.layout.item_recyclerview_layout,
        parent,
        false
    ))

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemRecyclerviewBinding.itemModel = items[position]
    }

    inner class ViewHolder( val itemRecyclerviewBinding : ItemRecyclerviewLayoutBinding) : RecyclerView.ViewHolder(itemRecyclerviewBinding.root)

}