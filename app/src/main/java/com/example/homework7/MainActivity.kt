package com.example.homework7

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var adapter:RecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        getData()
    }

    private fun getData() {
        DataLoader.getRequest("5edb4d643200002a005d26f0", object: CustomCallback{
            override fun onSuccess(result: String) {
                var myResult= "{\"data\":$result}";
                val model = Gson().fromJson(myResult, DataModel::class.java)

                init(model)
            }
        })
    }

    private fun init(model:DataModel) {
        recyclerView.layoutManager = LinearLayoutManager(this)
        adapter = RecyclerViewAdapter(model.data, this)
        recyclerView.adapter = adapter
    }
}
