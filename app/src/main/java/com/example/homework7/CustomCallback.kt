package com.example.homework7

interface CustomCallback {
    fun onSuccess(result: String){}
    fun onFailure(errorMassage: String){}
}