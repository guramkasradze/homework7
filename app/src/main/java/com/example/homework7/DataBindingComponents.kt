package com.example.homework7

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

object DataBindingComponents {
    @JvmStatic
    @BindingAdapter("setImage")
    fun setImage(view: ImageView, url: String) {
        Glide.with(view.context).load(url).into(view)
    }
}